/*
 * C++ Application for Ajile PS development.
 *
 * Purpose:
 * A extensible command line tool for testing different functions.
 *
 * Usage:
 * When a PS function needs to be tested, write a test function and attach
 * it to a command
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xparameters.h"
#include "xqspips.h"		/* QSPI device driver */

#include "xil_types.h"
#include "xuartps_hw.h"

#include "SerialCommand.h"
#include "qspi_int_util.h"


//some command
#include "command.h"
/************************** Constant Definitions *****************************/


/************************** Global Variable *********************************/
SerialCommand sCmd;


u32 FlashReadBaseAddress = 0;


/****For read flash****/
extern XScuGic IntcInstance;
extern XQspiPs QspiInstance;


int main()
{
	int Status;

	//Init QSPI flash
	Status = QspiInit(&IntcInstance, &QspiInstance, QSPI_DEVICE_ID, QSPI_INTR_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("InitQspi failed \n\r");
		return XST_FAILURE;
	}

	GPIOInit();

	//Print hint
	xil_printf("Ajile UART command utility\n");
	xil_printf("Implemented command: HELLO,DF,FRID,WF,EF,BP,BL,UF\n");
	xil_printf("For HelloWorld, DumpFlash,FlasReadID,WriteFlash,EraseFlash,\nButtonPress,ToggleLED,UploadFile\n");


	init_platform();


	sCmd.addCommand("HELLO", SayHello); // Echos the string argument back
	sCmd.addCommand("DF",DumpFlash);
	sCmd.addCommand("FRID",ReadFlashId);
	sCmd.addCommand("WF",WriteFlash);
	sCmd.addCommand("BP",ButtonPress);
	sCmd.addCommand("BL",ToggleLed);
	sCmd.addCommand("UF",WritePackets);
	sCmd.addCommand("WFFF",WriteFlashFF);
	sCmd.addCommand("LP", LongPress);
	sCmd.addCommand("EF",EraseFlashCmd);
	sCmd.setDefaultHandler(unrecognized);
   // print("Hello World\n\r");
    while(1)
    {
    	sCmd.readSerial();
    }
	return 0;
}









