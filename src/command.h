/*
 * command.h
 *
 *  Created on: 2015��1��12��
 *      Author: jchen
 */

#ifndef COMMAND_H_
#define COMMAND_H_

//LED
#define ledpin 47
//Press button
#define pbsw 51

//For flash write
#define START 1
#define ADDED 3



void SayHello();
void unrecognized(const char *command);
void DumpFlash();
void ReadFlashId();
void WriteFlash();
void WriteFlashFF();
void EraseFlashCmd();
int GPIOInit();
void ButtonPress();
void ToggleLed();
void WritePackets();
void LongPress();
void BlinkLedFewTimes();

/*************************/
void EraseFlash(u16 offset_as_sector);
void BlinkLedMore(u32 interval, int counts, bool forever);
#endif /* COMMAND_H_ */
