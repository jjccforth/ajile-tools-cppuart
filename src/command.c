/*
 * command.c
 *
 *  Created on: 2015��1��12��
 *      Author: jchen
 */
#include <stdlib.h>
#include <unistd.h>
#include "xuartps_hw.h"
#include "command.h"
#include "SerialCommand.h"
#include "qspi_int_util.h"
//For GPIO
#include "xgpiops.h"

extern SerialCommand sCmd;


/****For read flash****/
extern u8 ReadBuffer[MAX_DATA + DATA_OFFSET + DUMMY_SIZE];
extern u8 WriteBuffer[];
extern XScuGic IntcInstance;
extern XQspiPs QspiInstance;

/******for GPIO********/
static XGpioPs Gpio;
static XGpioPs_Config *GPIOConfigPtr;
bool ledOn;


bool ProcessPacket(u8 * input);
/******************************************************************************
* This command  is for test purpose only
*
* @param	none
*
* @note		None.
******************************************************************************/
void SayHello() {
	char *arg;
	arg = sCmd.next(); // Get the next argument from the SerialCommand object buffer
	if (arg != NULL) { // As long as it existed, take it
		print("Hello ");
		print(arg);
		print("\n");
	}
	else {
		print("Hello, whoever you are\n");
	}
}

/******************************************************************************
* This command  is for processing all unrecognized input
*
* @param	command
*
* @note		None.
******************************************************************************/
void unrecognized(const char *command) {
	print("unrecognized. Valid command: HELLO, DF, FRID,,WF,BP,BL\n");
}

/******************************************************************************
* This command  is for printing/displaying the flash data. command can follow a
* optional parameter as offset address of flash data want to peep.
*
* @param	none
*
* @note		None.
******************************************************************************/

void DumpFlash(){
	char *offset, * length;
	offset = sCmd.next();
	length = sCmd.next();
	u8 *BufferPtr;
	int i;
	unsigned long loffset;

	if (offset == NULL)
		loffset = 0;
	else
		loffset = strtol (offset,NULL,10);
	printf("dump flash from %lu , size:%s\n",loffset,length);
	memset(ReadBuffer, 0x00, sizeof(ReadBuffer));

	FlashRead(&QspiInstance, loffset, 256, READ_CMD);

	/*
	 * Setup a pointer to the start of the data that was read into the read
	 * buffer and verify the data read is the data that was written
	 */
	BufferPtr = &ReadBuffer[DATA_OFFSET];

	for(int seg = 0;seg <16;seg++){
		xil_printf("%02d:",seg);
		for(i = 0;i<16;i++)
			xil_printf("0x%02x ", BufferPtr[seg*16+i] );
		print("\n");
	}
	print("\n");
	print("\nAbove is dumped data");
}

/******************************************************************************
* This command  reads FlashID and prints brand, size information
*
* @param	none
*
* @note		None.
******************************************************************************/

void ReadFlashId(){
	u32 status = FlashReadID();
	if (status != XST_SUCCESS) {
		printf("FlashReadID() failed");
	}

}




/******************************************************************************
* This command  write flash with some pattern
*
* @param	none
*
* @note		None.
******************************************************************************/
void WriteFlash(){
	u8 uv;
	int Count;
	char *offset;
	unsigned long loffset;

	offset = sCmd.next();


	if (offset == NULL)
		loffset = 0;
	else
		loffset = strtol (offset,NULL,10);

	loffset = ((loffset-1)/PAGE_SIZE +1) *256; //Make sure loffset is on the boundry of 256

	for (uv = START, Count = 0; Count < PAGE_SIZE;
	     Count++, uv++) {
		WriteBuffer[DATA_OFFSET + Count] = (u8)(uv + ADDED);
	}
	//FlashErase(&QspiInstance, loffset, PAGE_SIZE);
	FlashWrite(&QspiInstance, loffset, PAGE_SIZE, WRITE_CMD);
	printf("write flash done, use DF to check,offset: %lu\n",loffset);
}

/**************************************************
 *  For testing a function only
 *************************************************/

void WriteFlashFF(){
	u8 uv;
	int Count;
	char *offset;
	unsigned long loffset;

	offset = sCmd.next();


	if (offset == NULL)
		loffset = 0;
	else
		loffset = strtol (offset,NULL,10);

	loffset = ((loffset-1)/PAGE_SIZE +1) *256; //Make sure loffset is on the boundry of 256

	for (uv = START, Count = 0; Count < PAGE_SIZE;
	     Count++, uv++) {
		WriteBuffer[DATA_OFFSET + Count] = 0x0;
	}
	//FlashErase(&QspiInstance, loffset, PAGE_SIZE);
	FlashWrite(&QspiInstance, loffset, PAGE_SIZE, WRITE_CMD);
	printf("write flash wif FF is done, use DF to check,offset: %lu\n",loffset);
}


/******************************************************************************
* This command  erase flash
*
* @param	1st number: offset as sector
*
* @note		None.
******************************************************************************/
void EraseFlashCmd(){

	char *offset_assector;
	u32 noffset_assector;

	offset_assector = sCmd.next();
	if (offset_assector == NULL)
		noffset_assector = 0;
	else
		noffset_assector = strtol (offset_assector,NULL,10);
	//Call erase flash function
	EraseFlash(noffset_assector);

	printf("erase flash done, use DF to check,offset: %lu\n",noffset_assector*PAGE_SIZE);
}

/******************************************************************************
* This function  erase the flash
*
* @param	offset_as_sector: the offset, as unit of a sector , i.e. 64k bytes
*
* @note		None.
******************************************************************************/
void EraseFlash(u16 offset_as_sector){
	unsigned long loffset;
	loffset = offset_as_sector * SECTOR_SIZE;
	FlashErase(&QspiInstance, loffset, PAGE_SIZE);
}

/******************************************************************************
* This function  initialize GPIO
*
* @param	none
*
* @note		None.
******************************************************************************/
int GPIOInit()
{
	int Status;
	//Init GPIO
	GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	Status = XGpioPs_CfgInitialize(&Gpio, GPIOConfigPtr, GPIOConfigPtr ->BaseAddr);
	if (Status != XST_SUCCESS) {
		xil_printf("Init GPIO failed \n\r");
		return XST_FAILURE;
	}
	XGpioPs_SetDirectionPin(&Gpio, ledpin, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, ledpin, 1);

	return XST_SUCCESS;

}


void ButtonPress(){
	printf("buttonPress called,waiting for press\n");
	u32 sw;
	u32 count = 0;
    while(1)
    {
    	usleep(50000); //50 millisecond
    	sw = XGpioPs_ReadPin(&Gpio, pbsw);
    	if(sw ==1 ){
    		printf("Button pressed,break, count %lu\n",count);
    		break;
    	}
    	count++;
    }
}

//
#define LONG_WAIT_SECONDS	5
#define SECTORS_TO_ERASE	10
#define BLINK_INTERVAL		600
void LongPress(){
	//printf("long press testing,no press 4 sec, pressed but released, long press\n");
	u32 sw;
	u32 count = 0;
	u32 pressCount = 0;
	u8 current_state = 0, old_state = 0;

	bool observed = false;
    while(1)
    {
    	usleep(50000); //50 millisecond sleep in side a loop. for each count increment
    	sw = XGpioPs_ReadPin(&Gpio, pbsw);
    	old_state = current_state;
    	current_state = sw;
    	if(sw ==1 ){
    		if(pressCount == 0)
    			observed = true;
    		pressCount++;

    	}


    	count++;
    	if( !observed && count>80){ //4 second passed
    		print("Button not pressed, more than 4 sec passed\n");
    		break;
    	}

    	//pressed more than 5 seconds
    	if((current_state == 1) && (pressCount > LONG_WAIT_SECONDS * 20)){ //20 count increment is 1s
    		print("Button pressed more than 5 sec\n");
    		break;
    	}


    	// if state changes, and the button is already pressed for some time
    	if((old_state != current_state) && pressCount > 2 ){ // at lease 100 millis
    		print("Button state changed, has been pressed\n");
    		observed = false;
    		break;
    	}

    }

    xil_printf("count: %d, pCnt: %d, current_state:%d\n",count,pressCount,current_state);
    if(observed){
    	for(count = 0 ;count < SECTORS_TO_ERASE; count ++ )
    		EraseFlash(count);
    	BlinkLedMore(BLINK_INTERVAL,1000,true);
    }


}

void BlinkLedFewTimes(){
	int count;
	char * sCount;
	sCount = sCmd.next();

	if (sCount == NULL)
		count = 0;
    else
		count = strtol (sCount,NULL,10);

	BlinkLedMore(600,count,false);
}

/******************************************************************************
* This command  toggle LED once
*
* @param	none
*
* @note		None.
******************************************************************************/


void ToggleLed(){
	ledOn = !ledOn;
	if(ledOn)
		XGpioPs_WritePin(&Gpio, ledpin, 0x1);
	else
		XGpioPs_WritePin(&Gpio, ledpin, 0x0);
}



/******************************************************************************
* This command  blink LED with more control
*
* @param	interval: how long between toggle, in milliseconds
* 			counts: how many times to blink. Only when forever is false
* 			forever: if true the blink will continue forever.
*
* @note		None.
******************************************************************************/
void BlinkLedMore(u32 interval, int counts, bool forever){
	u32 sleep = interval * 1000L;
	for (int i = 0;i<counts;i++)
	{

		ToggleLed();
		usleep(sleep);
		ToggleLed();
		usleep(sleep);
		if(forever == true)
			i--;
	}
}


/******************************************************************************
* This command  echo back to UART client with certain protocol.
*
* @param	none
*
* @note		There communication will use fixed size packets. A packet with the
* 			start byte of ETX will finish the communication and return to the
* 			command interpreter.
* 			The first parameter of ECHO command will be the offset of flash writing
* 			in PAGE_SIZE. i.e. 1 means 256, 2 means 512, etc.
*
*
******************************************************************************/
//Overhead, 1 tag, 2 byte seq (in page), 2 bytes total length in page, 2 bytes offset in sector
#define PACKET_OVERHEAD 7
#define PACKET_SIZE PACKET_OVERHEAD+PAGE_SIZE
#define ETX			0x53
#define STX			0x50
#define RESPONSE_END	0x45
u8 inputString[PACKET_SIZE];
void WritePackets(){
    u8 *inputStrPtr = inputString;

    u16 count = 0;

    print("Upload File called\n");
    while(1)
    {


 	   inputStrPtr = inputString + count;
    	//Get a byte
	   *inputStrPtr = XUartPs_RecvByte(STDOUT_BASEADDRESS);
	   count++;
	   //count %= 256; //Dont go over the boundary
	   //Process incoming message
	   if (count == PACKET_SIZE ) {
		   count = 0;
		   if(ProcessPacket(inputString)){
			   print("End of UploadFile, get out of cycle\n");
			   break;
		   }
	   }

    }
}

/******************************************************************************
* This function is used to process the packet.
*
* @param	input, the pointer to the packet buffer.
*
* @return	true for stop, false for not stop. control how packets coming
* @note		it will be a prototype for flash programming function
*
* communication protocol:
* The PC will send continuous packets with fixed size. Packets include a head and a payload of 256 bytes data
* packet format:
* incoming
* |STX|SEQ|LEN|OFFSET_SEC|DATA........| <- for all packets except last one. STX:1byte of start, SEQ, 2 byte of sequential no. DATA:256 bytes of payload
* |ETX|SEQ|LEN|OFFSET_SEC|DATA........| <- last packet
*
* outgoing
* |ACK|SEQ|	<- ACK:1 byte acknowledge which is STX or ETX +1, SEQ: 2 bytes of SEQ that received
*
******************************************************************************/
#define SEQ_OFFSET 		1
#define LEN_OFFSET		3
#define OFFSET_OFFSET	5

bool ProcessPacket(u8 * input){
	bool stop = false;
	u16 index = (u16) *(input +SEQ_OFFSET); //For calculating the offset(as page) of data for flash write
	u16	length_page = (u16) *(input + LEN_OFFSET); //Length in pages
	u16 offset_sec = (u16) *(input + OFFSET_OFFSET); // Offset sector
	//check the offset information
	//u32 offset = (offset_page + index) * 256;
	u32 offset_byte = index * PAGE_SIZE; //From the sector start
	u16 length_byte = length_page * PAGE_SIZE; //Total
	u32 sec_start =  offset_sec * SECTOR_SIZE;

	if(index == 0)
		FlashErase(&QspiInstance, sec_start , length_byte);

	memcpy(WriteBuffer+DATA_OFFSET,input+PACKET_OVERHEAD,PAGE_SIZE);
	//Write to flash
	FlashWrite(&QspiInstance, sec_start + offset_byte, PAGE_SIZE, WRITE_CMD);

	if(input[0] == ETX) //Last packet
		stop = true;

	//response with 3  bytes of data to get next packet
	XUartPs_SendByte(STDOUT_BASEADDRESS, input[0]+1); //tag+1
	XUartPs_SendByte(STDOUT_BASEADDRESS, input[1]);	  //sequence
	XUartPs_SendByte(STDOUT_BASEADDRESS, input[2]);

	//xil_printf("%04lu ",index);
	return stop;
}
