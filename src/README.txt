This is a basic extensible command line tools for a bare metal system as a tool for testing.

Any new function can be added into the command system so it can be tested with an interactive way.

Note:
* Before flash writing, the content should be erased first
* Flash writing should align with the boundary of 256 bytes 